﻿using UnityEngine;
using System.Collections;

public class Hazard : MonoBehaviour {

	// Flag to avoid doing additional checks.
	// Once triggered, further testing is avoided.
	private bool triggered = false;
	
	public bool Triggered {
		get {
			return triggered;
		}
	}

	void OnTriggerEnter2D(Collider2D other) {
		TestCollision(other);
	}

	void OnCollisionEnter2D(Collision2D other) {
		TestCollision(other.collider);
	}

	private void TestCollision(Collider2D other) {
		if (!triggered) {
			triggered = TestPlayerCollision(other);
		}
	}

	private bool TestPlayerCollision(Collider2D other) {
		PlayerCharacter character = other.gameObject.GetComponent<PlayerCharacter>();
		if (character != null && character.Alive) {
			character.Kill();
			return true;
		}

		return false;
	}

}
