﻿using UnityEngine;
using System.Collections;

public class PipeSpawner : ContinuousSpawner {
	
	public float spawnOffsetX = 1.0f;
	
	public float spawnMinY = 0.0f;
	public float spawnMaxY = 0.0f;

	public ScoreController scoreController = null;
	
	private float SpawnOffsetX {
		get {
			return (spawnOffsetX < 0.0f ? spawnObject.renderer.bounds.size.x : spawnOffsetX);
		}
	}
	
	protected override bool ShouldSpawn {
		get {
			return (lastSpawnedObject.transform.position.x + SpawnOffsetX) < gameObject.transform.position.x;
		}
	}

	protected override Vector3 SpawnPosition {
		get {
			Vector3 position = (lastSpawnedObject == null ?
		        gameObject.transform.position :
		        new Vector3(
					lastSpawnedObject.transform.position.x + SpawnOffsetX,
					lastSpawnedObject.transform.position.y,
					lastSpawnedObject.transform.position.z
				)
	        );

			position.y = Random.Range(spawnMinY, spawnMaxY);
			return position;
		}
	}

	protected override GameObject Spawn() {
		GameObject newObject = base.Spawn();

		ScoreTrigger trigger = newObject.GetComponentInChildren<ScoreTrigger>();
		if (trigger != null) {
			trigger.scoreController = scoreController;
		}

		return newObject;
	}
}
