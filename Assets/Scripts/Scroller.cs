using UnityEngine;
using System.Collections;

public class Scroller : MonoBehaviour {
	
	public float scrollSpeed = 1f;
	public Vector2 scrollDirection = new Vector2(-1.0f, 0.0f);

	public bool isScrolling = true;

	void Awake() {
		// Since scrollDirection is a direction vector,
		// ensure that it is normalized
		scrollDirection.Normalize();
	}

	// Update is called once per frame
	void Update() {
		if (isScrolling) {
			transform.Translate(scrollDirection * scrollSpeed * Time.deltaTime);
		}
	}
}
