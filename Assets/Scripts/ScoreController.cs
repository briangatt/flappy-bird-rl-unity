﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ScoreController : MonoBehaviour
{
	public int initialScore = 0;

	public Sprite[] digits = new Sprite[10];
	public string sortingLayerName;
	
	private int score = -1;

	void Awake() {
		Score = initialScore;
	}

	public int Score {
		get {
			return score;
		}
		
		set {
			if (score != value) {
				score = value;
				RenderScore();
			}
		}
	}

	private List<GameObject> InstantiateScore(int score) {
		List<GameObject> scoreSprites = new List<GameObject>();

		if (score < 1) {
			scoreSprites.Add(CreateGameObjectFromSprite(digits[0], sortingLayerName, gameObject));
		} else {
			for (int div = score; div > 0; div /= 10) {
				int remainder = div % 10;
				scoreSprites.Insert(0, CreateGameObjectFromSprite(digits[remainder], sortingLayerName, gameObject));
			}
		}

		return scoreSprites;
	}

	private static GameObject CreateGameObjectFromSprite(Sprite sprite, string sortingLayerName, GameObject parent) {
		GameObject value = new GameObject();
		value.name = sprite.name;

		SpriteRenderer renderer = value.AddComponent<SpriteRenderer>();
		renderer.sprite = sprite;

		if (sortingLayerName != null) {
			renderer.sortingLayerName = sortingLayerName;
		}

		if (parent != null) {
			value.transform.parent = parent.transform;
			value.layer = parent.layer;
		}
		
		return value;
	}
	
	private void RenderScore() {
		Clear();
		RenderScore(score);
	}

	private void Clear() {
		for (int i = 0; i < gameObject.transform.childCount; ++i) {
			Destroy(gameObject.transform.GetChild(i).gameObject);
		}
	}

	private void RenderScore(int score) {
		RenderScore(InstantiateScore(score));
	}

	private void RenderScore(List<GameObject> digits) {
		float totalWidth = 0.0f;
		foreach (GameObject digit in digits) {
			totalWidth += digit.renderer.bounds.size.x;
		}

		// Center score
		Vector3 position = new Vector3(-(totalWidth / 2.0f), 0.0f, 0.0f);
		foreach (GameObject digit in digits) {
			// The position vector is taken relative to the parent GameObject
			digit.transform.localPosition = position;
			position.x += digit.renderer.bounds.size.x;
		}
	}
}