﻿using UnityEngine;
using System.Collections;

public class ScoreTrigger : MonoBehaviour {

	public int scoreIncrement = 1;
	//public string scoreControllerGameObjectName = "Score";
	public ScoreController scoreController = null;

	private bool triggered = false;

	public bool Triggered {
		get {
			return triggered;
		}
	}

	void Awake() {
		// Possibly avoided via Singleton pattern
		// Reference: http://stackoverflow.com/questions/3136008/is-this-singleton-implementation-correct-and-thread-safe
		//GameObject scoreBoard = GameObject.Find(scoreControllerGameObjectName);
		//if (scoreBoard != null) {
		//	scoreController = scoreBoard.GetComponent<ScoreController>();
		//}
	}

	public void OnTriggerEnter2D(Collider2D other) {
		PlayerCharacter bird = other.gameObject.GetComponent<PlayerCharacter>();

		if (bird != null && bird.Alive && !Triggered) {
			scoreController.Score += scoreIncrement;

			if (audio != null && GameController.Instance.sound) {
				audio.Play();
			}

			triggered = true;
		}
	}

}
